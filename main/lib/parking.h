#ifndef PARKING_H
#define PARKING_H

#include <Arduino.h>
#include <string>


class Parking{

public :     
    
    // attributs
    String id;
    String name; 
    float longitude;
    float latitude;
    
    
    // constructeur
    Parking(String id, String name, float longitude, float latitude );


    // méthodes 
    String buildURL();
   
    






};






#endif
