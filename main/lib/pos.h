#ifndef POS_H
#define POS_H

#include <Arduino.h>
#include <string>


class Pos{
  public: 
      float longi;
      float lati;
      
      Pos(float longi, float lati);

      String buildURLApi(String source, String destination);
  
};


#endif
