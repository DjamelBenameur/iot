#include <Arduino.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>
#include <ESP8266WiFiMulti.h>
#include <string.h>
#include <algorithm>



#include "lib/parking.h"
#include "lib/pos.h"

//changer ssid avec nom du réseau et password avec le mot de passe du réseau
const char* ssid = "DjamelBen";
const char* password = "12345678";

const uint8_t fingerprint[20] = {0x40, 0xaf, 0x00, 0x6b, 0xec, 0x90, 0x22, 0x41, 0x8e, 0xa3, 0xad, 0xfa, 0x1a, 0xe8, 0x25, 0x41, 0x1d, 0x1a, 0x54, 0xb3};

ESP8266WiFiMulti WiFiMulti; 
std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure);

// Données de géolocalisation : https://data.montpellier3m.fr/dataset/parkings-en-ouvrage-de-montpellier
// Identifiants : https://data.montpellier3m.fr/dataset/disponibilite-des-places-dans-les-parkings-de-montpellier-mediterranee-metropole


Parking parkings[] = {
  /*       ID,           name,                      longitude,         latitude   */
  { "FR_MTP_ANTI",  "Antigone",                   3.888818930000000, 43.608716059999999 },
  { "FR_MTP_COME",  "Comédie",                    3.879761960000000, 43.608560920000002 },
  { "FR_MTP_CORU",  "Corum",                      3.882257730000000, 43.613888209999999 },
  { "FR_MTP_EURO",  "Europa",                     3.892530740000000, 43.607849710000004 },
  { "FR_MTP_FOCH",  "Foch Préfecture",            3.876570840000000, 43.610749120000001 },
  { "FR_MTP_GAMB",  "Gambetta",                   3.871374360000000, 43.606951379999998 },
  { "FR_MTP_GARE",  "Saint Roch",                 3.878550720000000, 43.603291489999997 },
  { "FR_MTP_TRIA",  "Triangle",                   3.881844180000000, 43.609233840000002 },
  { "FR_MTP_ARCT",  "Arc de Triomphe",            3.873200750000000, 43.611002669999998 },
  { "FR_MTP_PITO",  "Pitot",                      3.870191170000000, 43.612244939999997 },
  { "FR_MTP_CIRC",  "Circé Odysseum",             3.917849500000000, 43.604953770000002 },
  { "FR_MTP_SABI",  "Sabines",                    3.860224600000000, 43.583832630000003 },
  { "FR_MTP_GARC",  "Garcia Lorca",               3.890715800000000, 43.590985089999997 },
  { "FR_CAS_SABL",  "Notre Dame de Sablassou",    3.922295360000000, 43.634191940000001 },
  { "FR_MTP_MOSS",  "Mosson",                     3.819665540000000, 43.616237159999997 },
  { "FR_STJ_SJLC",  "Saint-Jean-le-Sec",          3.837931200000000, 43.570822249999999 },
  { "FR_MTP_MEDC",  "Euromédecine",               3.827723650000000, 43.638953590000000 },
  { "FR_MTP_OCCI",  "Occitanie",                  3.848597960000000, 43.634562320000001 },
  { "FR_CAS_CDGA",  "Charles de Gaulle",          3.897762100000000, 43.628542119999999 },
  { "FR_MTP_ARCE",  "Arceaux",                    3.867490670000000, 43.611716469999998 },
  { "FR_MTP_POLY",  "Polygone",                   3.884765390000000, 43.608370960000002 },
  { "FR_MTP_GA109", "Multiplexe (est)",           3.918980000000000, 43.605060000000000 },
  { "FR_MTP_GA250", "Multiplexe (ouest)",         3.914030000000000, 43.604000000000000 },
};


// Récupère dans le xml les chiffre entre <Free> et </Free>
int getPlacesVides(String line) {
  return line.substring(line.indexOf("<Free>") + 6, line.indexOf("</Free>")).toInt();

}




// Retourne le nombre de place vide dans un parking 
int getNombreDePlacesVides(Parking parking){
 HTTPClient http; 
    if (http.begin(*client, parking.buildURL())) // HTTPS
        {
            int start = http.GET(); // start connection and send HTTP header
            int nbPlace=0;
            if (start > 0)            // httpCode will be negative on error
            {
                if (start == HTTP_CODE_OK || start == HTTP_CODE_MOVED_PERMANENTLY)
                {
                    nbPlace = getPlacesVides(http.getString());
                }
               
            }
            return nbPlace; 
            http.end();
        }
}



// Version utilisant un calcul direct
float calcDistanceDirect(float userLon, float parkLon, float userLat, float parkLat) {
  return sqrt(pow(parkLon - userLon, 2) + pow(parkLat - userLat, 2));
}


// Retourne le parking le plus proche à cette utilisateur.
Parking parkingProcheDirect(Pos positionUser) {
  float bestDist = -1;
  int posArray = -1;
  float calc;
  size_t taille = sizeof(parkings) / sizeof(parkings[0]);
  for (int i = 0; i < taille; ++i) {
    if (getNombreDePlacesVides(parkings[i]) > 0) {
      calc = calcDistanceDirect(positionUser.longi, parkings[i].longitude, positionUser.lati, parkings[i].latitude);
      if (bestDist == -1 || calc < bestDist) {
        bestDist = calc;
        posArray = i;
      }
    }
  }
  if(posArray == -1){
    return {"null","null",0.0,0.0};
  }
  else{
    return parkings[posArray];
  }
}



// création de l'url de la requête
String buildURLApi(String source, String destination){
  String path = "";
  path.concat("https://router.project-osrm.org/route/v1/car/");
  path.concat(source);
  path.concat(";");
  path.concat(destination);
  return path;  
}


// Version utilisant un calcul de distance via API GPS
float calcDistanceAPI(float userLon, float parkLon, float userLat, float parkLat) {
  String source, destination, query, res;
  source = "";
  destination = "";
  // élaboration de la source et destination
  source.concat(userLon);
  source.concat(",");
  source.concat(userLat);
  destination.concat(parkLon);
  destination.concat(",");
  destination.concat(parkLat);
  // création url requête
  query = buildURLApi(source,destination);
  res = getDistanceAPI(query);
  // récupération de la distance dans la réponse API
  res = res.substring(res.indexOf("distance"));
  res = res.substring(res.indexOf("distance"));
  res = res.substring(res.indexOf("distance"));
  //il y a 3 attributs distance, le bon est le 4eme
  res = res.substring(res.indexOf("distance" + 3),res.indexOf(","));
  //retourne distance
  return res.toFloat();
} 


Parking parkingProcheAPI(Pos positionUser) {
  float bestDist = -1;
  int posArray = -1;
  float calc;
  size_t taille = sizeof(parkings) / sizeof(parkings[0]);
  for (int i = 0; i < taille; ++i) {
    if (getNombreDePlacesVides(parkings[i]) > 0) {
      calc = calcDistanceAPI(positionUser.longi, parkings[i].longitude, positionUser.lati, parkings[i].latitude);
      // si calc < 0 alors erreur lors de la connexion api
      if (calc >= 0 && (bestDist == -1 || calc < bestDist)) {
        bestDist = calc;
        posArray = i;
      }
    }
  }
  //si -1 alors aucune place disponible ou alors erreur
  if(posArray == -1){
    return {"null","null",0.0,0.0};
  }
  else {
    return parkings[posArray];
  }
}


// récupérer le distance en utilisant l'API 
String getDistanceAPI(String coord){
   HTTPClient http2;
   
    if (http2.begin(*client, coord)) // HTTPS
        {
            int start = http2.GET(); // start connection and send HTTP header
            String distance;
            if (start > 0)
                       // httpCode will be negative on error
            {
            
                if (start == HTTP_CODE_OK || start == HTTP_CODE_MOVED_PERMANENTLY)
                {
                  distance = http2.getString();
                }
            }
           
            return distance;
            http2.end(); 
            
        }
  //Serial.print("Echec connexion à " + coord + "\n");
  //return "-1.0"; 
}






void setup() {
  Serial.begin(9600);

  for (uint8_t t = 4; t > 0; t--)
  {
    Serial.printf("[SETUP] WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }

  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP(ssid, password);

  while (WiFiMulti.run() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println("WiFi connected");


}

void loop() {
  Serial.println("**************************** Début de programme *********************");
  
  if (!WiFiMulti.run() == WL_CONNECTED) {
    Serial.println("[HTTPS] Unable to connect\n");
    delay(5000);
    return;
  }


  client -> setFingerprint(fingerprint);
  // or 
  // client -> setInsecure();

  Pos positionUser = Pos(3.888818930000000, 43.608716059999999);
  Parking pDirect = parkingProcheDirect(positionUser);
  Parking pApi = parkingProcheAPI(positionUser);

  if(pDirect.id.equals("null")){
    Serial.print("aucune place disponible lors du calcul distance à vol d'oiseau");
  }
  else{
    Serial.print("Distance à vol d'oiseau: le parking le plus proche est: " + pDirect.name);
  }
  
  Serial.println("\n");
  
  if(pApi.id.equals("null")){ 
    Serial.print("aucune place disponible lors du calcul distance via API");
  }
  else{
    Serial.print("Distance via API: le parking le plus proche est: " + pApi.name);
  }

  
   
  
  Serial.println("\n");
  Serial.println("[ Prochains résultats dans 5 minutes ... ]");
  delay(300000);
}
