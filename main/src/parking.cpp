#include"../lib/parking.h"
#include "../lib/pos.h"

#define MONTPELLIER3M_BASE_URL "https://data.montpellier3m.fr/"
#define MONTPELLIER3M_API_PATH_PREFIX "sites/default/files/ressources/"
#define MONTPELLIER3M_API_PATH_SUFFIX ".xml"


/*
Une classe qui implémente un parking avec comme paramètres l'identifiant d'un parking, le nom d'un parking
et la position du parking en longitude et latitude.

Méthode : 
- buildURL : qui permet de construire l'URl des fichiers XML de chaque parking récupérer depuis DATA MONTPELLIER 



*/


Parking::Parking(String id, String name, float longitude, float latitude){
    this-> id= id;
    this-> name = name;
    this-> longitude = longitude;
    this-> latitude = latitude;

};


String Parking::buildURL(){
    String res = MONTPELLIER3M_BASE_URL MONTPELLIER3M_API_PATH_PREFIX;
    res += this-> id;
    res += MONTPELLIER3M_API_PATH_SUFFIX;
    return res; 

}
